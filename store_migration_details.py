import redis
import os
import json
from botocore.vendored import requests

def get_crons(event, context):
    """
    Authenticate User with Doosync and get the Ongoing Crons
    """

    authenticate_url = os.environ['authenticate_url']
    body = {
            'email': os.environ['email'],
            'password': os.environ['password']
        }

    headers = {'Content-type': 'application/json'}

    authenticate_response = requests.post(url=authenticate_url, json=body, headers=headers).json()

    authenticate_headers = {
        'Authorization' : authenticate_response['token']
        }
    crons_url= os.environ['ongoing_crons_url']

    ongoing_crons = requests.get(url=crons_url,headers=authenticate_headers).json()

    return ongoing_crons

def store_progress(event, context):
    
    ongoing_crons = get_crons(event, context)

    retail_progess_url = os.environ['retail_progess_url']
    
    active_user_ids = {
        'userIds': ongoing_crons['cronsRunning']
        }

    all_migration_ids_details = requests.post(url=retail_progess_url,json=active_user_ids).json()
    
    redis_url = os.environ['redis_url']  
    r = redis.StrictRedis.from_url(redis_url, db=0)
    
    for migration_ids in all_migration_ids_details:
        if 'ZohoTicket' in all_migration_ids_details[migration_ids]:
            print('Zoho Key Already Exists')
            
            migration_details = all_migration_ids_details[migration_ids]
            values = json.dumps(migration_details)
            r.set(migration_ids,values,ex=14400)  
        else:
            all_migration_ids_details[migration_ids]['ZohoTicket'] = False
            
            migration_details = all_migration_ids_details[migration_ids]
            values = json.dumps(migration_details)
            r.set(migration_ids,values,ex=14400)
    
    print(r.keys())
        
    return 'Success!'
