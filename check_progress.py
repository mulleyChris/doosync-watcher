import redis
import json
from botocore.vendored import requests
import os


def check_progress(event, context):
    """
    
    """

    authenticate_url = os.environ['authenticate_url']
    body = {
            'email': os.environ['email'],
            'password': os.environ['password']
        }

    headers = {'Content-type': 'application/json'}

    authenticate_response = requests.post(url=authenticate_url, json=body, headers=headers).json()

    authenticate_headers = {
        'Authorization' : authenticate_response['token']
    }
    crons_url= os.environ['ongoing_crons_url']

    ongoing_crons = requests.get(url=crons_url,headers=authenticate_headers).json()

    retail_progess_url = os.environ['retail_progess_url'] 
    active_user_ids = {'userIds' : ongoing_crons['cronsRunning']}

    all_migration_ids_details = requests.post(url=retail_progess_url,json=active_user_ids).json()    
    redis_url = os.environ['redis_url']  
    r = redis.StrictRedis.from_url(redis_url, db=0)
    
    for migration_ids in all_migration_ids_details:
        is_running = False
        
        current_keys = all_migration_ids_details[migration_ids]
        last_count = json.loads(r.get(migration_ids))    
        
        entity_keys = ['items', 'customers', 'sales', 'vendors', 'itemmatrixs', 'giftcards', 'creditaccountbalances', 'creditaccounts', 'categorys', 'manufacturers', 'inventoryadjustments']
            
        for entity in entity_keys:
            
            last_count_progress = last_count[entity]['progress']
            current_count_progress = current_keys[entity]['progress']
            
            #comparing the dicts
            if last_count_progress != current_count_progress:
                is_running = True

        if is_running == False:
            
            if last_count['ZohoTicket'] == True:
                print('ZohoTicket Already Created')
            else:
                print('Migration Stopped',migration_ids)
                print(entity," last_count_progress :",last_count_progress, "current_count_progress",current_count_progress)

                getByStoreId  = os.environ['getByStoreId'] + migration_ids
                zoho = os.environ['zoho']

                get_customer_headers={
                    'Authorization' : authenticate_response['token'],
                    'Content-type': 'application/json'
                }
                #Get Customer Info By Migration ID
                get_customer_response = requests.get(url=getByStoreId,  headers=get_customer_headers).json()

                #Customer Info
                account_name = get_customer_response['contact']['name']
                po_number = get_customer_response['poNumber']
                phone_num = get_customer_response['contact']['phone']
                name = get_customer_response['contact']['primary_contact']
                email = get_customer_response['email']

                account_body = {
                    'functionToExecute':'fetchOrCreateAccount',
                    'functionArguments':{
                        'accountName': account_name,
                        'description': 'TEST'
                    }
                }

                #Account Request
                account_response = requests.post(url=zoho, json=account_body, headers=get_customer_headers)
                
                account_response = account_response.json()

                customer_args = {
                    'title': 'FFA-' + str(po_number),
                    'phoneNumber': phone_num,
                    'clientName': name,
                    'clientEmail': email
                }

                contact_body = {
                    'functionToExecute': 'fetchOrCreateContact',
                    'functionArguments': customer_args
                }

                #Contact Request
                zoho_contact = requests.post(url=zoho, json=contact_body, headers=authenticate_headers)
                
                zoho_contact = zoho_contact.json()

                ticket_args = {
                    'contactId': zoho_contact['id'],
                    'subject': 'The Migration Has Stopped :(',
                    'departmentId': '496136000000416165', # level 2 departement
                    'description': 'The migration for ' + account_name + ' (' + email + ')' + ' has stopped',
                    'priority': 'Medium'
                }
                ticket_body = {
                    'functionToExecute': 'createTicket',
                    'functionArguments': ticket_args
                }

                #Create Ticket
                ticket_response = requests.post(url=zoho, json=ticket_body, headers=authenticate_headers).json()
            
    return "Good Job"