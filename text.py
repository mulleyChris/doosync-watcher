import redis
import os

def handler (event, context):
  redis_url = os.environ['redis_url']
  r = redis.StrictRedis.from_url(redis_url)
  print(r.keys())
  return "Good"
